# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :app,
  ecto_repos: [App.Repo]

# Configures the endpoint
config :app, AppWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "UjMrtxv88eyEzwLzU+lDh1W2EWIirMfNaVEaHP/uLijkFI5l5b3MzlhT51XZHYpa",
  render_errors: [view: AppWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: App.PubSub,
  live_view: [signing_salt: "9Qb+DKWh"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :app, AppWeb.Plugs.Auth.Guardian,
       issuer: "app",
       secret_key: "h3kyXHjfULHII6Ns8vGKruZBif1Vlobl6X1hfQfE0f4hueIY0HaxGOpe9ARHBb8o",
       ttl: {14, :days}

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
