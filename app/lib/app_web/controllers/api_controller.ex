defmodule AppWeb.ApiController do
  use AppWeb, :controller
  alias App.NetworkElements
  alias App.NetworkElements.Edge
  alias App.NetworkElements.Lane
  alias App.NetworkElements.LaneShape

  def map(conn, _params) do
    lanes = NetworkElements.list_lanes()
    map_list = []
    map_list = for i <- lanes do
      edge = NetworkElements.get_edge_by_id(i.edge_id)
      lane_map = %{"lane_id" => i.id, "edge_id" => edge.id, "lanes" => edge.lane_number, "street_name" => edge.street_name}
      {:ok, %{rows: rows, columns: columns}} = NetworkElements.get_lane_shape_by_lane_id(i.id)
      shape_map = Enum.flat_map(rows, fn x -> [Enum.zip(columns, x)|>Enum.into(%{})] end)
      lane_map = Map.put_new(lane_map, "shape", shape_map)
      map_list = List.insert_at(map_list, 0, lane_map)
    end
    json(conn, map_list)
  end

  def lane(conn, %{"lane_id"=> lane_id}) do
    resp = %{"data"=> lane_id}
    elastic_url = "http://localhost:9200"
    {:ok, resp} = Elastix.Search.search(elastic_url, "network-data", ["_doc"], %{"query" => %{"match" => %{"lane_id" => lane_id}}, "size" => "1", "sort" => %{"timestamp" => "desc"}})
    IO.inspect(resp.body["hits"]["hits"])
    json(conn, List.first(resp.body["hits"]["hits"])["_source"]["counter"])
  end

  def lane_history(conn, %{"lane_id"=> lane_id}) do
    resp = %{"data"=> lane_id}
    IO.inspect(lane_id)
    elastic_url = "http://localhost:9200"
    {:ok, resp} = Elastix.Search.search(elastic_url, "network-data", ["_doc"], %{"query" => %{"match" => %{"lane_id" => lane_id}}, "size" => "15", "sort" => %{"timestamp" => "desc"}})
    counter_list = []
    counter_list = for i <- resp.body["hits"]["hits"] do
      counter_list = List.insert_at(counter_list, 0, i["_source"]["counter"])
    end
    counter_list = List.flatten(counter_list)
    timestamp_list = []
    timestamp_list = for i <- resp.body["hits"]["hits"] do
      timestamp_list = List.insert_at(timestamp_list, 0, i["_source"]["timestamp"])
    end
    IO.inspect(resp.body["hits"]["hits"])
    timestamp_list = List.flatten(timestamp_list)
    chart_resp = %{"time"=> timestamp_list, "values" => counter_list}
    json(conn, chart_resp)
  end
end