defmodule AppWeb.DashboardController do
  use AppWeb, :controller

  plug :put_layout, "internal.html"

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
