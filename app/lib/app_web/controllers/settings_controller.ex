defmodule AppWeb.SettingsController do
  use AppWeb, :controller
  alias App.Repo
  alias App.Accounts
  alias App.Accounts.User
  alias App.NetworkElements
  alias App.NetworkElements.Edge
  alias App.NetworkElements.Lane
  alias App.NetworkElements.LaneShape

  plug :put_layout, "internal.html"
  plug AppWeb.Plugs.AuthorizeAdmin

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new_user(conn, _params) do
    roles = Accounts.list_roles()
    changeset = Accounts.change_user(%User{})
    render(conn, "new_user.html", changeset: changeset, roles: roles)
  end

  def edit_user(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    roles = Accounts.list_roles()
    render(conn, "edit_user.html", user: user, changeset: changeset, roles: roles)
  end

  def change_password(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    roles = Accounts.list_roles()
    render(conn, "password_user.html", user: user, changeset: changeset, roles: roles)
  end

  def delete_user(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    {:ok, user} =Accounts.delete_user(user)
    conn
    |> put_flash(:info, "User removed!")
    |> redirect(to: Routes.settings_path(conn, :index))
  end

  def update_user(conn,%{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)
    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User updated!")
        |> redirect(to: Routes.settings_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit_user.html", user: user, changeset: changeset)
    end
  end

  def create_user(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User created!")
        |> redirect(to: Routes.settings_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        roles = Accounts.list_roles()
        render(conn, "new_user.html", changeset: changeset, roles: roles)
    end
  end

  def remove_configuration(conn, _params) do
    NetworkElements.delete_configuration
    conn
    |> put_flash(:info, "Configuration deleted!")
    |> redirect(to: Routes.settings_path(conn, :index))
  end

  def load_configuration(conn, params) do
    if (Map.has_key?(params, "edge") and Map.has_key?(params, "lane") and Map.has_key?(params, "shape")) do
      try do
        NetworkElements.delete_configuration
        params["edge"].path
        |>File.stream!()
        |>CSV.decode(headers: [:street_name, :id, :lane_number])
        |>Enum.map(fn (item) ->
          {:ok, fields} = item
          Edge.changeset(%Edge{}, fields)|> Repo.insert end)

        params["lane"].path
        |>File.stream!()
        |>CSV.decode(headers: [:id, :edge_id])
        |>Enum.map(fn (item) ->
          {:ok, fields} = item
          Lane.changeset(%Lane{}, fields)|> Repo.insert end)

        params["shape"].path
        |>File.stream!()
        |>CSV.decode(headers: [:id, :pos_x, :pos_y, :lane_id])
        |>Enum.map(fn (item) ->
          {:ok, fields} = item
          LaneShape.changeset(%LaneShape{}, fields)|> Repo.insert end)

        conn
        |> put_flash(:success, "Files imported!")
        |> redirect(to: Routes.settings_path(conn, :index))
      rescue
        error ->
          IO.inspect(error)
          NetworkElements.delete_configuration
          conn
          |> put_flash(:error, "Missing or invalid file!")
          |> redirect(to: Routes.settings_path(conn, :index))
      end
    else
      conn
      |> put_flash(:error, "Missing or invalid file!")
      |> redirect(to: Routes.settings_path(conn, :index))
    end
  end
end
