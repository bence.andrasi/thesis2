defmodule AppWeb.RoadController do
  use AppWeb, :controller
  alias App.Repo
  alias App.NetworkElements

  plug :put_layout, "internal.html"

  def index(conn, _params) do
    {:ok, %{rows: rows, columns: columns}} = NetworkElements.get_road_list()
    roads = Enum.flat_map(rows, fn x -> [Enum.zip(columns, x)|>Enum.into(%{})] end)
    render(conn, "index.html", roads: roads)
  end

end
