defmodule AppWeb.SessionController do
  use AppWeb, :controller
  import Ecto.Query
  alias App.Accounts.User
  alias App.Accounts.Hashing
  alias App.Repo
  alias AppWeb.Plugs.Auth.Guardian

  def new(conn, _params) do
    maybe_user = Guardian.Plug.current_resource(conn)
    if maybe_user do
      conn |> redirect(to: Routes.dashboard_path(conn, :index))
    else
      render conn, "login.html"
    end
  end

  def login(conn, params) do
    username = params["username"]
    password = params["password"]
    authenticate(username, password)
    |> login_reply(conn)
  end

  defp login_reply({:ok, user}, conn) do
    conn
    |> put_flash(:info, "Welcome back!")
    |> Guardian.Plug.sign_in(user)
    |> redirect(to: Routes.dashboard_path(conn, :index))
  end

  defp login_reply({:error, reason}, conn) do
    conn
    |> put_flash(:error, to_string(reason))
    |> redirect(to: Routes.session_path(conn, :new))
  end

  defp authenticate(username, plain_text_password) do
    query = from u in User, where: u.username == ^username
    case Repo.one(query) do
      nil ->
        {:error, :invalid_credentials}
      user ->
        if Hashing.validate_password(plain_text_password, user.password_hash) do
          {:ok, user}
        else
          {:error, :invalid_credentials}
        end
    end
  end

  def logout(conn, _params) do
    conn
    |> Guardian.Plug.sign_out()
    |> put_flash(:info, "Signed out!")
    |> redirect(to: Routes.session_path(conn, :new))
  end
end