defmodule AppWeb.SettingsView do
  use AppWeb, :view

  def get_user_pill(id) do
    case id do
      1 -> raw('<span class="span-admin">Admin</span>')
      2 -> raw('<span class="span-user">Operator</span>')
      _ -> ''
    end
  end

end
