defmodule AppWeb.Plugs.AuthorizeAdmin do
  use AppWeb, :controller
  import Plug.Conn

  def init(default), do: default

  def call(conn, _params)do
    if Guardian.Plug.current_resource(conn).role_id != 1 do
      conn
      |> redirect(to: Routes.dashboard_path(conn, :index))
    else
      conn
    end
  end
end