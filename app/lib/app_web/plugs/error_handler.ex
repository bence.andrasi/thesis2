defmodule AppWeb.Plugs.Auth.ErrorHandler do
  use AppWeb, :controller
  import Plug.Conn

  @behaviour Guardian.Plug.ErrorHandler

  @impl Guardian.Plug.ErrorHandler
  def auth_error(conn, {type, _reason}, _opts) do
    body = to_string(type)
    conn
    |> put_flash(:error, body)
    |> clear_session
    |> redirect(to: Routes.session_path(conn, :new))
  end
end