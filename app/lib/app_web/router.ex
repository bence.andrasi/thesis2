defmodule AppWeb.Router do
  use AppWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :auth do
    plug AppWeb.Plugs.Auth.Pipeline
  end

  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated
  end

  scope "/", AppWeb do
    pipe_through [:browser, :auth]

    get "/", SessionController, :new
    get "/logout", SessionController, :logout
    post "/login", SessionController, :login

    pipe_through :ensure_auth
    get "/dashboard", DashboardController, :index
    get "/roads", RoadController, :index
    scope "/settings" do
      get "/", SettingsController, :index
      get "/user", SettingsController, :new_user
      post "/user", SettingsController, :create_user
      post "/user/:id/delete", SettingsController, :delete_user
      get "/user/:id/edit", SettingsController, :edit_user
      put "/user/:id", SettingsController, :update_user
      get "/user/:id/password", SettingsController, :change_password
    end

    post "/settings/configuration", SettingsController, :load_configuration
    post "/settings/configuration/remove", SettingsController, :remove_configuration
  end

  scope "/api", AppWeb do
    pipe_through [:api, :auth, :ensure_auth]
    get "/map", ApiController, :map
    get "/lane-info/:lane_id", ApiController, :lane
    get "/lane-history/:lane_id", ApiController, :lane_history
  end

  # Other scopes may use custom stacks.
  # scope "/api", AppWeb do
  #   pipe_through :api
  # end
end
