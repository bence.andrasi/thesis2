defmodule App.NetworkElements do
  @moduledoc """
  The NetworkElements context.
  """

  import Ecto.Query, warn: false
  alias App.Repo

  alias App.NetworkElements.Edge

  @doc """
  Returns the list of edges.

  ## Examples

      iex> list_edges()
      [%Edge{}, ...]

  """
  def list_edges do
    Repo.all(Edge)
  end

  @doc """
  Gets a single edge.

  Raises `Ecto.NoResultsError` if the Edge does not exist.

  ## Examples

      iex> get_edge!(123)
      %Edge{}

      iex> get_edge!(456)
      ** (Ecto.NoResultsError)

  """
  def get_edge!(id), do: Repo.get!(Edge, id)

  @doc """
  Creates a edge.

  ## Examples

      iex> create_edge(%{field: value})
      {:ok, %Edge{}}

      iex> create_edge(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_edge(attrs \\ %{}) do
    %Edge{}
    |> Edge.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a edge.

  ## Examples

      iex> update_edge(edge, %{field: new_value})
      {:ok, %Edge{}}

      iex> update_edge(edge, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_edge(%Edge{} = edge, attrs) do
    edge
    |> Edge.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a edge.

  ## Examples

      iex> delete_edge(edge)
      {:ok, %Edge{}}

      iex> delete_edge(edge)
      {:error, %Ecto.Changeset{}}

  """
  def delete_edge(%Edge{} = edge) do
    Repo.delete(edge)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking edge changes.

  ## Examples

      iex> change_edge(edge)
      %Ecto.Changeset{data: %Edge{}}

  """
  def change_edge(%Edge{} = edge, attrs \\ %{}) do
    Edge.changeset(edge, attrs)
  end

  alias App.NetworkElements.Lane

  @doc """
  Returns the list of lanes.

  ## Examples

      iex> list_lanes()
      [%Lane{}, ...]

  """
  def list_lanes do
    Repo.all(Lane)
  end

  @doc """
  Gets a single lane.

  Raises `Ecto.NoResultsError` if the Lane does not exist.

  ## Examples

      iex> get_lane!(123)
      %Lane{}

      iex> get_lane!(456)
      ** (Ecto.NoResultsError)

  """
  def get_lane!(id), do: Repo.get!(Lane, id)

  @doc """
  Creates a lane.

  ## Examples

      iex> create_lane(%{field: value})
      {:ok, %Lane{}}

      iex> create_lane(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_lane(attrs \\ %{}) do
    %Lane{}
    |> Lane.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a lane.

  ## Examples

      iex> update_lane(lane, %{field: new_value})
      {:ok, %Lane{}}

      iex> update_lane(lane, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_lane(%Lane{} = lane, attrs) do
    lane
    |> Lane.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a lane.

  ## Examples

      iex> delete_lane(lane)
      {:ok, %Lane{}}

      iex> delete_lane(lane)
      {:error, %Ecto.Changeset{}}

  """
  def delete_lane(%Lane{} = lane) do
    Repo.delete(lane)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking lane changes.

  ## Examples

      iex> change_lane(lane)
      %Ecto.Changeset{data: %Lane{}}

  """
  def change_lane(%Lane{} = lane, attrs \\ %{}) do
    Lane.changeset(lane, attrs)
  end

  alias App.NetworkElements.LaneShape

  @doc """
  Returns the list of lane_shapes.

  ## Examples

      iex> list_lane_shapes()
      [%LaneShape{}, ...]

  """
  def list_lane_shapes do
    Repo.all(LaneShape)
  end

  @doc """
  Gets a single lane_shape.

  Raises `Ecto.NoResultsError` if the Lane shape does not exist.

  ## Examples

      iex> get_lane_shape!(123)
      %LaneShape{}

      iex> get_lane_shape!(456)
      ** (Ecto.NoResultsError)

  """
  def get_lane_shape!(id), do: Repo.get!(LaneShape, id)

  @doc """
  Creates a lane_shape.

  ## Examples

      iex> create_lane_shape(%{field: value})
      {:ok, %LaneShape{}}

      iex> create_lane_shape(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_lane_shape(attrs \\ %{}) do
    %LaneShape{}
    |> LaneShape.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a lane_shape.

  ## Examples

      iex> update_lane_shape(lane_shape, %{field: new_value})
      {:ok, %LaneShape{}}

      iex> update_lane_shape(lane_shape, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_lane_shape(%LaneShape{} = lane_shape, attrs) do
    lane_shape
    |> LaneShape.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a lane_shape.

  ## Examples

      iex> delete_lane_shape(lane_shape)
      {:ok, %LaneShape{}}

      iex> delete_lane_shape(lane_shape)
      {:error, %Ecto.Changeset{}}

  """
  def delete_lane_shape(%LaneShape{} = lane_shape) do
    Repo.delete(lane_shape)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking lane_shape changes.

  ## Examples

      iex> change_lane_shape(lane_shape)
      %Ecto.Changeset{data: %LaneShape{}}

  """
  def change_lane_shape(%LaneShape{} = lane_shape, attrs \\ %{}) do
    LaneShape.changeset(lane_shape, attrs)
  end

  def get_edge_by_id(id), do: Repo.get_by!(Edge, id: id)

  def get_lane_shape_by_lane_id(id) do
    Ecto.Adapters.SQL.query(
      Repo,
      """
      select pos_x, pos_y from lane_shapes where lane_id = $1::text order by id DESC
      """, [id]
    )
  end

  def get_road_list() do
    Ecto.Adapters.SQL.query(
      Repo,
      """
      select street_name, count(id) from (select * from edges) a LEFT JOIN (select edge_id, count(id) from lanes group by edge_id) b on (a.id = b.edge_id) group by street_name
      """
    )
  end

  def delete_configuration do
    Ecto.Adapters.SQL.query(
      Repo,
      """
      delete from edges;
      """
    )

    Ecto.Adapters.SQL.query(
      Repo,
      """
      delete from lane_shapes;
      """
    )

    Ecto.Adapters.SQL.query(
      Repo,
      """
      delete from lanes;
      """
    )
  end
end
