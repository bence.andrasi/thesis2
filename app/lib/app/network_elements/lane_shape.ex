defmodule App.NetworkElements.LaneShape do
  use Ecto.Schema
  import Ecto.Changeset

  schema "lane_shapes" do
    field :pos_x, :float
    field :pos_y, :float
    field :lane_id, :string

    timestamps()
  end

  @doc false
  def changeset(lane_shape, attrs) do
    lane_shape
    |> cast(attrs, [:pos_x, :pos_y, :lane_id])
    |> validate_required([:pos_x, :pos_y, :lane_id])
  end
end
