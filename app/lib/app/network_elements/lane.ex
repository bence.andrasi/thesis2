defmodule App.NetworkElements.Lane do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :string, []}
  schema "lanes" do
    field :edge_id, :string
    timestamps()
  end

  @doc false
  def changeset(lane, attrs) do
    lane
    |> cast(attrs, [:id, :edge_id])
    |> validate_required([:id, :edge_id])
  end
end
