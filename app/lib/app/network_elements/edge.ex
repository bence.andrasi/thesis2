defmodule App.NetworkElements.Edge do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :string, []}
  schema "edges" do
    field :lane_number, :integer
    field :street_name, :string

    timestamps()
  end

  @doc false
  def changeset(edge, attrs) do
    edge
    |> cast(attrs, [:street_name, :id, :lane_number])
    |> validate_required([:street_name, :id, :lane_number])
  end
end
