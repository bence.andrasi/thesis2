defmodule App.Accounts.PasswordReset do
  use Ecto.Schema
  import Ecto.Changeset

  schema "password_resets" do
    field :secret_hash, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(password_reset, attrs) do
    password_reset
    |> cast(attrs, [:secret_hash])
    |> validate_required([:secret_hash])
  end
end
