defmodule App.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias App.Accounts.Hashing

  schema "users" do
    field :email, :string
    field :password_hash, :string
    field :username, :string
    belongs_to :role, App.Accounts.Role
    ## virtual fields ##
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :password, :password_confirmation, :role_id])
    |> validate_required([:username, :email, :role_id])
    |> validate_format(:email, ~r/.+@.+\..+/, [message: "Invalid e-mail format!"])
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password)
    |> unique_constraint(:email)
    |> downcase_email
    |> hash_password
  end

  defp downcase_email(changeset) do
    update_change(changeset, :email, &String.downcase/1)
  end

  defp hash_password(changeset) do
    password = get_change(changeset, :password)
    if password do
      hashed_password = Hashing.hash_password(password)
      put_change(changeset, :password_hash, hashed_password)
    else
      changeset
    end
  end
end
