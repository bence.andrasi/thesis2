defmodule App.Repo.Migrations.CreateEdges do
  use Ecto.Migration

  def change do
    create table(:edges, primary_key: false) do
      add :street_name, :string
      add :id, :string, primary_key: true
      add :lane_number, :integer

      timestamps()
    end

  end
end
