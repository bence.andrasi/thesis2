defmodule App.Repo.Migrations.CreateLanes do
  use Ecto.Migration

  def change do
    create table(:lanes, primary_key: false) do
      add :id, :string, primary_key: true
      add :edge_id, references(:edges, column: :id, type: :string, on_delete: :nothing)

      timestamps()
    end

    create index(:lanes, [:edge_id])
  end
end
