defmodule App.Repo.Migrations.CreateLaneShapes do
  use Ecto.Migration

  def change do
    create table(:lane_shapes) do
      add :pos_x, :float
      add :pos_y, :float
      add :lane_id, references(:lanes, column: :id, type: :string, on_delete: :nothing)

      timestamps()
    end

    create index(:lane_shapes, [:lane_id])
  end
end
