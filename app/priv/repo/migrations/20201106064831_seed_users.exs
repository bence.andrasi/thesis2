defmodule App.Repo.Migrations.SeedUsers do
  use Ecto.Migration

  def change do
    App.Repo.insert!(
      %App.Accounts.User{
        username: "furiouscat67",
        email: "bence.andrasi95@gmail.com",
        password_hash: "$argon2id$v=19$m=131072,t=8,p=4$FZG1iX1x/KHQMktC01HZhw$Xjiz9uI5dGTISJQs1rEumJYz0YVRtXjsA59nmoAXd3I",
        role_id: 1
      }
    )
  end
end
