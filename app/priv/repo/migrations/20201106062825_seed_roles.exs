defmodule App.Repo.Migrations.SeedRoles do
  use Ecto.Migration

  def change do
    App.Repo.insert!(
      %App.Accounts.Role{
        name: "admin"
      }
    )

    App.Repo.insert!(
      %App.Accounts.Role{
        name: "operator"
      }
    )
  end
end
