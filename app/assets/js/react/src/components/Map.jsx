import React, { Component } from 'react';
import Chart from 'chart.js';

class TrafficChart extends Component{
    constructor(props) {
        super(props);
    }

    componentDidMount(){
        var ctx = document.getElementById('myChart');
        var times = this.props.labels.map(label => {
            var date = new Date(label);
            var sec = date.getSeconds();
            var min = date.getMinutes();
            var hour = date.getHours();
            return `${hour}:${min}:${sec}`
        })
        var myLineChart = new Chart(ctx, {
            type: "line",
            data: {
                labels: times,
                datasets: [
                    {
                        label: "Traffic History",
                        data: this.props.values,
                        backgroundColor: "#0652DD",
                        borderColor: "#009432",
                        borderWidth: 3
                    }
                ]
            }
        });
    }

    componentDidUpdate() {
        var ctx = document.getElementById('myChart');
        var times = this.props.labels.map(label => {
            var date = new Date(label);
            var sec = date.getSeconds();
            var min = date.getMinutes();
            var hour = date.getHours();
            return `${hour}:${min}:${sec}`
        })
        var myLineChart = new Chart(ctx, {
            type: "line",
            data: {
                labels: times,
                datasets: [
                    {
                        label: "Traffic History",
                        data: this.props.values,
                        backgroundColor: "#0652DD",
                        borderColor: "#009432",
                        borderWidth: 3
                    }
                ]
            }
        });
    }

    render(){
        return(
            <canvas id="myChart" width="200" height="150"></canvas>
        )
    }

}

class InfoPanel extends Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount(){
        fetch(`/api/lane-history/${this.props.data.lane_id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps != this.props){
            fetch(`/api/lane-history/${this.props.data.lane_id}`)
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            isLoaded: true,
                            items: result
                        });
                    },

                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )
        }
    }

    render() {
        if (this.props.data){
            const {error, isLoaded, items, laneData} = this.state;
            if (error) {
                return <div className='dashboard-map-info'>Error: {error.message}</div>
            } else if (!isLoaded) {
                return <div className='dashboard-map-info'>Loading...</div>
            } else {
                return (
                    <div className='dashboard-map-info'>
                        <h1>Information</h1>
                        <h2>Street name:</h2>
                        <p>{this.props.data.street_name}</p>
                        <h2>No. of lanes:</h2>
                        <p>{this.props.data.lanes}</p>
                        <h2>Lane ID:</h2>
                        <p>{this.props.data.lane_id}</p>
                        <h2>Edge ID:</h2>
                        <p>{this.props.data.edge_id}</p>
                        <TrafficChart labels={items.time} values={items.values}/>
                    </div>
                )
            }
        }
    }
}

class Lane extends Component{

    constructor(props) {
        super(props);
        this.points = this.generatePoints(this.props.data);
        this.defaultColor = this.generateDefaultColor(this.props.data);
        this.streetName = this.props.data.street_name;
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            width: 4,
            color: this.defaultColor
        };
        this.fetchData();
    }

    generateDefaultColor(data){
        return data.lane_id.startsWith('invert') ? "#0652DD" : "#009432"
    }

    fetchData(){
        fetch(`/api/lane-info/${this.props.data.lane_id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.updateState(result)
                },

                (error) => {
                    console.log(error)
                }
            )
    }

    updateState(result){
        if (result < 2){
            this.setState({
                width: 4,
                color: this.defaultColor
            });
        } else if (result >= 2 && result <=4){
            this.setState({
                width: 6,
                color: "#F79F1F"
            });
        }else{
            this.setState({
                width: 8,
                color: "red"
            });
        }
    }

    componentDidMount() {
        this.interval = setInterval(() => this.fetchData(), 10000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    handleClick() {
        this.props.mapCallback(this.props.data);
    }

    generatePoints (data){
        var points = ``;
        if (data.lane_id.startsWith('invert')){
            data.shape.map(shape => (points = points.concat(`${shape.pos_x},${600-shape.pos_y} `)));
        }else {
            data.shape.map(shape => (points = points.concat(`${shape.pos_x+4},${600-shape.pos_y+8} `)));
        }
        return points;
    }

    render(){
        const points = this.points;
        return(
            <g>
                <polyline
                    points={points}
                    fill="none"
                    strokeWidth={this.state.width}
                    stroke={this.state.color}
                    onClick={this.handleClick}
                />
            </g>
        )
    }
}

class Map extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            laneData: null,
            items: []
        };
        this.callbackFunction = this.callbackFunction.bind(this);
    }

    callbackFunction(data){
        this.setState({
            laneData: data
        });
    }

    componentDidMount() {
        fetch("/api/map")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const {error, isLoaded, items, laneData} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>
        } else if (!isLoaded) {
            return <div>Loading...</div>
        } else if (laneData){
            return (
                <div className='dashboard-map-app'>
                    <div className='dashboard-map-display'>
                        <svg width={600} height={600}>
                            {items.map(item => (
                                <Lane data={item[0]} key={item[0].lane_id} mapCallback={this.callbackFunction}/>
                            ))}
                        </svg>
                    </div>
                    <InfoPanel data={this.state.laneData}/>
                </div>
            )
        } else{
            return (
                <div className='dashboard-map-app'>
                    <div className='dashboard-map-display'>
                        <svg width={600} height={600}>
                            {items.map(item => (
                                <Lane data={item[0]} key={item[0].lane_id} mapCallback={this.callbackFunction}/>
                            ))}
                        </svg>
                    </div>
                    <div className='dashboard-map-info'>
                        <h1>Information</h1>
                        <p>Select a Lane to display data</p>
                    </div>
                </div>
            )
        }
    }
}
export default Map