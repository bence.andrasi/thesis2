defmodule App.NetworkElementsTest do
  use App.DataCase

  alias App.NetworkElements

  describe "edges" do
    alias App.NetworkElements.Edge

    @valid_attrs %{edge_id: "some edge_id", lane_number: 42, street_name: "some street_name"}
    @update_attrs %{edge_id: "some updated edge_id", lane_number: 43, street_name: "some updated street_name"}
    @invalid_attrs %{edge_id: nil, lane_number: nil, street_name: nil}

    def edge_fixture(attrs \\ %{}) do
      {:ok, edge} =
        attrs
        |> Enum.into(@valid_attrs)
        |> NetworkElements.create_edge()

      edge
    end

    test "list_edges/0 returns all edges" do
      edge = edge_fixture()
      assert NetworkElements.list_edges() == [edge]
    end

    test "get_edge!/1 returns the edge with given id" do
      edge = edge_fixture()
      assert NetworkElements.get_edge!(edge.id) == edge
    end

    test "create_edge/1 with valid data creates a edge" do
      assert {:ok, %Edge{} = edge} = NetworkElements.create_edge(@valid_attrs)
      assert edge.edge_id == "some edge_id"
      assert edge.lane_number == 42
      assert edge.street_name == "some street_name"
    end

    test "create_edge/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = NetworkElements.create_edge(@invalid_attrs)
    end

    test "update_edge/2 with valid data updates the edge" do
      edge = edge_fixture()
      assert {:ok, %Edge{} = edge} = NetworkElements.update_edge(edge, @update_attrs)
      assert edge.edge_id == "some updated edge_id"
      assert edge.lane_number == 43
      assert edge.street_name == "some updated street_name"
    end

    test "update_edge/2 with invalid data returns error changeset" do
      edge = edge_fixture()
      assert {:error, %Ecto.Changeset{}} = NetworkElements.update_edge(edge, @invalid_attrs)
      assert edge == NetworkElements.get_edge!(edge.id)
    end

    test "delete_edge/1 deletes the edge" do
      edge = edge_fixture()
      assert {:ok, %Edge{}} = NetworkElements.delete_edge(edge)
      assert_raise Ecto.NoResultsError, fn -> NetworkElements.get_edge!(edge.id) end
    end

    test "change_edge/1 returns a edge changeset" do
      edge = edge_fixture()
      assert %Ecto.Changeset{} = NetworkElements.change_edge(edge)
    end
  end

  describe "lanes" do
    alias App.NetworkElements.Lane

    @valid_attrs %{lane_id: "some lane_id"}
    @update_attrs %{lane_id: "some updated lane_id"}
    @invalid_attrs %{lane_id: nil}

    def lane_fixture(attrs \\ %{}) do
      {:ok, lane} =
        attrs
        |> Enum.into(@valid_attrs)
        |> NetworkElements.create_lane()

      lane
    end

    test "list_lanes/0 returns all lanes" do
      lane = lane_fixture()
      assert NetworkElements.list_lanes() == [lane]
    end

    test "get_lane!/1 returns the lane with given id" do
      lane = lane_fixture()
      assert NetworkElements.get_lane!(lane.id) == lane
    end

    test "create_lane/1 with valid data creates a lane" do
      assert {:ok, %Lane{} = lane} = NetworkElements.create_lane(@valid_attrs)
      assert lane.lane_id == "some lane_id"
    end

    test "create_lane/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = NetworkElements.create_lane(@invalid_attrs)
    end

    test "update_lane/2 with valid data updates the lane" do
      lane = lane_fixture()
      assert {:ok, %Lane{} = lane} = NetworkElements.update_lane(lane, @update_attrs)
      assert lane.lane_id == "some updated lane_id"
    end

    test "update_lane/2 with invalid data returns error changeset" do
      lane = lane_fixture()
      assert {:error, %Ecto.Changeset{}} = NetworkElements.update_lane(lane, @invalid_attrs)
      assert lane == NetworkElements.get_lane!(lane.id)
    end

    test "delete_lane/1 deletes the lane" do
      lane = lane_fixture()
      assert {:ok, %Lane{}} = NetworkElements.delete_lane(lane)
      assert_raise Ecto.NoResultsError, fn -> NetworkElements.get_lane!(lane.id) end
    end

    test "change_lane/1 returns a lane changeset" do
      lane = lane_fixture()
      assert %Ecto.Changeset{} = NetworkElements.change_lane(lane)
    end
  end

  describe "lane_shapes" do
    alias App.NetworkElements.LaneShape

    @valid_attrs %{pos_x: 120.5, pos_y: 120.5}
    @update_attrs %{pos_x: 456.7, pos_y: 456.7}
    @invalid_attrs %{pos_x: nil, pos_y: nil}

    def lane_shape_fixture(attrs \\ %{}) do
      {:ok, lane_shape} =
        attrs
        |> Enum.into(@valid_attrs)
        |> NetworkElements.create_lane_shape()

      lane_shape
    end

    test "list_lane_shapes/0 returns all lane_shapes" do
      lane_shape = lane_shape_fixture()
      assert NetworkElements.list_lane_shapes() == [lane_shape]
    end

    test "get_lane_shape!/1 returns the lane_shape with given id" do
      lane_shape = lane_shape_fixture()
      assert NetworkElements.get_lane_shape!(lane_shape.id) == lane_shape
    end

    test "create_lane_shape/1 with valid data creates a lane_shape" do
      assert {:ok, %LaneShape{} = lane_shape} = NetworkElements.create_lane_shape(@valid_attrs)
      assert lane_shape.pos_x == 120.5
      assert lane_shape.pos_y == 120.5
    end

    test "create_lane_shape/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = NetworkElements.create_lane_shape(@invalid_attrs)
    end

    test "update_lane_shape/2 with valid data updates the lane_shape" do
      lane_shape = lane_shape_fixture()
      assert {:ok, %LaneShape{} = lane_shape} = NetworkElements.update_lane_shape(lane_shape, @update_attrs)
      assert lane_shape.pos_x == 456.7
      assert lane_shape.pos_y == 456.7
    end

    test "update_lane_shape/2 with invalid data returns error changeset" do
      lane_shape = lane_shape_fixture()
      assert {:error, %Ecto.Changeset{}} = NetworkElements.update_lane_shape(lane_shape, @invalid_attrs)
      assert lane_shape == NetworkElements.get_lane_shape!(lane_shape.id)
    end

    test "delete_lane_shape/1 deletes the lane_shape" do
      lane_shape = lane_shape_fixture()
      assert {:ok, %LaneShape{}} = NetworkElements.delete_lane_shape(lane_shape)
      assert_raise Ecto.NoResultsError, fn -> NetworkElements.get_lane_shape!(lane_shape.id) end
    end

    test "change_lane_shape/1 returns a lane_shape changeset" do
      lane_shape = lane_shape_fixture()
      assert %Ecto.Changeset{} = NetworkElements.change_lane_shape(lane_shape)
    end
  end
end
