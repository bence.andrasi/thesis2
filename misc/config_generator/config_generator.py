
import sys
import os
sys.path.append(os.path.join(os.environ["SUMO_HOME"],"tools"))
import traci
import csv
from hashlib import sha256

traci.start(["sumo-gui", "-c", "/usr/local/Cellar/sumo/1.7.0/share/sumo/tools/sim2/mysim.sumocfg", "-S"])

'''
Generate lanes and lane_shapes tables
'''
lane_matrix = []
lane_matrix_orig = []
lane_shape_matrix = []
lane_ids = traci.lane.getIDList()
lane_shape_counter = 0
for lane_id in lane_ids:
    if ":" in lane_id:
        continue
    shape = traci.lane.getShape(lane_id)
    edge_id = traci.lane.getEdgeID(lane_id)
    lane_matrix_orig.append([lane_id, edge_id])
    lane_id = ("invert" + str(sha256(lane_id).hexdigest())[:24]) if lane_id.startswith('-') else str(sha256(lane_id).hexdigest())[:24]
    edge_id = ("invert" + str(sha256(edge_id).hexdigest())[:24]) if edge_id.startswith('-') else str(sha256(edge_id).hexdigest())[:24]
    lane_matrix.append([lane_id, edge_id])
    for o in shape:
        lane_shape_counter = lane_shape_counter + 1
        lane_shape_matrix.append([lane_shape_counter, float(o[0]), float(o[1]), lane_id])

# normalize coordinates in 0-600 interval

min_x = lane_shape_matrix[0][1]
max_x = lane_shape_matrix[0][1]
min_y = lane_shape_matrix[0][2]
max_y = lane_shape_matrix[0][2]

for i in range(0, len(lane_shape_matrix)):
    if lane_shape_matrix[i][1] < min_x:
        min_x = lane_shape_matrix[i][1]
    if lane_shape_matrix[i][1] > max_x:
        max_x = lane_shape_matrix[i][1]
    if lane_shape_matrix[i][2] < min_y:
        min_y = lane_shape_matrix[i][2]
    if lane_shape_matrix[i][2] > max_y:
        max_y = lane_shape_matrix[i][2]

abs_max_x = max_x + abs(min_x)
abs_max_y = max_y + abs(min_y)

for i in range(0, len(lane_shape_matrix)):
    temp_x = float(lane_shape_matrix[i][1])
    temp_y = float(lane_shape_matrix[i][2])
    temp_x = (((temp_x + abs(float(min_x))) / float(abs_max_x)) * 600)
    temp_y = (((temp_y + abs(float(min_y))) / float(abs_max_y)) * 600)
    lane_shape_matrix[i] = [lane_shape_matrix[i][0], temp_x, temp_y, lane_shape_matrix[i][3]]

with open("import_lanes.csv", "w+") as output:
    csvWriter = csv.writer(output, delimiter=',')
    csvWriter.writerows(lane_matrix)

with open("import_lane_shapes.csv", "w+") as output:
    csvWriter = csv.writer(output, delimiter=',')
    csvWriter.writerows(lane_shape_matrix)


'''
Generate edge table based on lanes
'''
edge_set = set()
edge_matrix = []
for lane_data in lane_matrix_orig:
    edge_id = lane_data[1]
    edge_set.add(edge_id)

for edge_id in edge_set:
    street_name = traci.edge.getStreetName(edge_id)
    lane_count = traci.edge.getLaneNumber(edge_id)
    edge_id = ("invert" + str(sha256(edge_id).hexdigest())[:24]) if edge_id.startswith('-') else str(sha256(edge_id).hexdigest())[:24]
    edge_matrix.append([street_name, edge_id, lane_count])

with open("import_edges.csv", "w+") as output:
    csvWriter = csv.writer(output, delimiter=',')
    csvWriter.writerows(edge_matrix)
