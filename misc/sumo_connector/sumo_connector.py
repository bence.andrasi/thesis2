import sys,os
sys.path.append(os.path.join(os.environ["SUMO_HOME"],"tools"))
import traci
import time
from datetime import datetime
import socket
import json
from hashlib import sha256

host = 'localhost'
port = 9999
traci.start(["sumo-gui", "-c", "/usr/local/Cellar/sumo/1.7.0/share/sumo/tools/sim2/mysim.sumocfg", "-S"])
step = 0

lane_ids = traci.lane.getIDList()
while step < 1000:
    traci.simulationStep()
    for i in lane_ids:
        if ":" in i:
            continue
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        counter = len(traci.lane.getLastStepVehicleIDs(i))
        lane_app_id = ("invert" + str(sha256(i).hexdigest())[:24]) if i.startswith('-') else str(sha256(i).hexdigest())[:24]
        data = {'timestamp': datetime.utcnow().isoformat(), 'lane_id': lane_app_id, 'counter': counter}
        s.sendall(bytes(json.dumps(data)))
        s.close()
    step += 1
    time.sleep(5)
traci.close(True)